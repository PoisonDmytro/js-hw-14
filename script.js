let dayColor = {
    color1 : "red",
    color2 : "yellow",
    color3 : "black",
    colorbtn : "white"
}
let nightColor = {
    color1 : "grey",
    color2 : "blue",
    color3 : "white",
    colorbtn : "black"
}
let colors = {};
//----------------------
let switchbtn = document.querySelector('.switchbtn')
let theme = localStorage.getItem("theme");
if (theme == null){theme = "day";}
if(theme == "day"){
    colors = dayColor;
    switchbtn.textContent = "Day"
}else{
    colors = nightColor;
    switchbtn.textContent = "Night";
}
//------------
getColors();
//-----------
switchbtn.addEventListener("click", (event) => {
    if(theme == "day"){
        switchbtn.textContent = "Night";
        theme = "night";
        colors = nightColor;
        localStorage.setItem("theme", "night");
        getColors()
    }else{
        switchbtn.textContent = "Day";
        theme = "day";
        colors = dayColor;
        localStorage.setItem("theme", "day");
        getColors()
    }
    
});
function getColors(){
    let switchbtn = document.querySelector('.switchbtn');
    let container = document.querySelector('.container');
    let element = document.querySelectorAll(".element");

    switchbtn.style.backgroundColor = `${colors.colorbtn}`;
    switchbtn.style.color = `${colors.color3}`;
    container.style.backgroundColor = `${colors.color1}`;
    for(el of element){
        el.style.backgroundColor = `${colors.color2}`;
    }
}
